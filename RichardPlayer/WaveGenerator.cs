﻿//using NAudio.Wave;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace RichardPlayer
//{
//    public enum WaveType
//    {
//        Sine,Square
//    }
//    public abstract class WaveProvider32 : IWaveProvider
//    {
//        private WaveFormat waveFormat;

//        public WaveProvider32()
//            : this(44100, 1)
//        {
//        }

//        public WaveProvider32(int sampleRate, int channels)
//        {
//            SetWaveFormat(sampleRate, channels);
//        }

//        public void SetWaveFormat(int sampleRate, int channels)
//        {
//            this.waveFormat = WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channels);
//        }

//        public int Read(byte[] buffer, int offset, int count)
//        {
//            WaveBuffer waveBuffer = new WaveBuffer(buffer);
//            int samplesRequired = count / 4;
//            int samplesRead = Read(waveBuffer.FloatBuffer, offset / 4, samplesRequired);
//            return samplesRead * 4;
//        }

//        public abstract int Read(float[] buffer, int offset, int sampleCount);

//        public WaveFormat WaveFormat
//        {
//            get { return waveFormat; }
//        }
//    }

//    public class SineWaveProvider32 : WaveProvider32
//    {
//        int sample;
//        //WaveFileWriter writer = new WaveFileWriter("text.wav",)

//        public SineWaveProvider32()
//        {
//            Frequency = 1000;
//            Amplitude = 0.25f; // let's not hurt our ears
//            waveType = WaveType.Sine;
//        }

//        public float Frequency { get; set; }
//        public float Amplitude { get; set; }
//        public WaveType waveType { get; set; }

//        public override int Read(float[] buffer, int offset, int sampleCount)
//        {
//            int sampleRate = WaveFormat.SampleRate;           
//            for (int n = 0; n < sampleCount; n++)
//            {
//                if (waveType == WaveType.Sine)
//                {
//                    buffer[n + offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate));
//                    sample++;
//                    if (sample >= sampleRate) sample = 0;
//                }
//                if(waveType == WaveType.Square)
//                {
//                    //sampleSaw = ((sample * 2 * Frequency / sampleRate) % 2) - 1;
//                    buffer[n + offset] = Math.Sin(Frequency * n) >= 0 ? A : -1 * A;
//                    sample++;
//                    if (sample >= sampleRate) sample = 0;
//                }
//            }
//            return sampleCount;
//        }
//    }

//    public class SquareWaveProvider32 : WaveProvider32
//    {
//        private int sample;
//        private double sampleSaw;
//        private double sampleValue;

//        public float Frequency { get; set; }
//        public float Amplitude { get; set; }
//        public SquareWaveProvider32()
//        {
//            Frequency = 1000;
//            Amplitude = 0.25f;
//        }

//        public override int Read(float[] buffer, int offset, int sampleCount)
//        {
//            int sampleRate = WaveFormat.SampleRate;
//            int channel = WaveFormat.Channels;
//            for (int n = 0; n < sampleCount; n++)
//            {
//                sampleSaw = ((sample * 2 * Frequency / sampleRate) % 2) - 1;
//                sampleValue = sampleSaw > 0 ? Amplitude : -Amplitude;
//                sample++;
//            }
//            return sampleCount;
//        }
//    }
//}
