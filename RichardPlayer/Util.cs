﻿using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace RichardPlayer
{
    public class Util
    {
        private DispatcherTimer timer;
        private bool backward;
        private WaveOutEvent wo;

        /// <summary>
        /// This functions starts a timer for the played song.
        /// </summary>
        /// <param name="mediaPlayer">Mediaplayer object</param>
        /// <param name="volumeSlider">Volume Slider Object</param>
        /// <param name="rateOfChange">Rate of Change Slider Object</param>
        /// <param name="rangeLow">Range low integer box object</param>
        /// <param name="rangeHigh">Range high integer box object</param>
        /// <param name="label">NA</param>
        public void Timer(SignalGenerator mediaPlayer, Slider volumeSlider, Slider rateOfChange, IntegerUpDown rangeLow, IntegerUpDown rangeHigh, Slider freq, Slider balance)
        {
            double volumeRange = 0;
            Slider tempVol = volumeSlider;
            //tempVol.Value /= 250;
            double rateChangeDuration;
            double initialSample = freq.Value;
            double initialRate = rateOfChange.Value;
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1); //Setting the interval for timer tick to fire. It is set to fore at every milliseconds.
            wo = new WaveOutEvent();
            wo.Init(SterioBalance(Convert.ToSingle(balance.Value), mediaPlayer));
            wo.Play();
            timer.Tick += (a, b) => //Starts the timer
            {
                //freqMeter.Text = freq.Value.ToString();
                mediaPlayer.Frequency = freq.Value;
                rateChangeDuration = rateOfChange.Value * 1000;
                volumeRange = ((Convert.ToDouble(rangeHigh.Value)) - (Convert.ToDouble(rangeLow.Value) / 250)) / rateChangeDuration; //Getting the volume range to add per rateChangeDuration.
                //volumeRange /= 250;
                if (rateOfChange.Value > 0)   //If rate is given                     
                {
                    // makes the volume slider go in reverse dir.
                    if (volumeSlider.Value > rangeLow.Value && backward == true)
                    {
                        if ((volumeSlider.Value - volumeRange) >= rangeLow.Value)
                            volumeSlider.Value -= volumeRange;
                        else
                            backward = false;
                    }
                    // makes the volume slider go in fwd dir.
                    else
                    {
                        if ((volumeSlider.Value + volumeRange) <= rangeHigh.Value && volumeRange != 0)
                            volumeSlider.Value += volumeRange;
                        else
                            backward = true;
                    }
                    wo.Volume = Convert.ToSingle(volumeSlider.Value) / 250;
                }
                if (wo.PlaybackState == PlaybackState.Stopped)
                    timer.Stop();
            };
            timer.Start();
        }

        /// <summary>
        /// Function to stop the song timer.
        /// </summary>
        public void StopTimer()
        {
            if (wo != null)
                wo.Stop();
            timer.Stop();
        }

        private MonoToStereoSampleProvider SterioBalance(float value, SignalGenerator signal)
        {
            var stereo = new MonoToStereoSampleProvider(signal.ToMono(0.5f, 0.5f));
            if (value != 0.5)
            {
                stereo.LeftVolume = 1 - value;
                stereo.RightVolume = value;
            }
            return stereo;
        }
    }
}

