﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using NAudio.Wave.SampleProviders;
using NAudio.Wave;
using System.Windows.Threading;

namespace RichardPlayer.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Entry Point of the application. This initialise the window at startup.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            CheckUIChange();
        }
        private void Song1_Clicked(object sender, RoutedEventArgs e)
        {
            if (Song1.IsChecked == true)
            {
                sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq1.Value
                };
                if (form1.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form1.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form1.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form1.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util1.Timer(sine20Seconds, Volume1, RateOfChange1, RangeLow1, RangeHigh1, freq1, Balance1);
            }
            else
            {
                util1.StopTimer();
            }
        }
        private void Song2_Checked(object sender, RoutedEventArgs e)
        {
            if (Song2.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq2.Value
                };
                if (form2.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form2.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form2.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form2.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util2.Timer(sine20Seconds, Volume2, RateOfChange2, RangeLow2, RangeHigh2, freq2, Balance2);
            }
            else
            {
                util2.StopTimer();
            }
        }
        private void Song3_Checked(object sender, RoutedEventArgs e)
        {
            if (Song3.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq3.Value
                };
                if (form3.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form3.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form3.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form3.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util3.Timer(sine20Seconds, Volume3, RateOfChange3, RangeLow3, RangeHigh3, freq3, Balance3);
            }
            else
            {
                util3.StopTimer();
            }
        }
        private void Song4_Checked(object sender, RoutedEventArgs e)
        {
            if (Song4.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq4.Value,
                };
                if (form4.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form4.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form4.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form4.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util4.Timer(sine20Seconds, Volume4, RateOfChange4, RangeLow4, RangeHigh4, freq4, Balance4);
            }
            else
            {
                util4.StopTimer();
            }
        }
        private void Song5_Checked(object sender, RoutedEventArgs e)
        {
            if (Song5.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq5.Value
                };
                if (form5.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form5.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form5.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form5.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util5.Timer(sine20Seconds, Volume5, RateOfChange5, RangeLow5, RangeHigh5, freq5, Balance5);
            }
            else
            {
                util5.StopTimer();
            }
        }
        private void Song6_Checked(object sender, RoutedEventArgs e)
        {
            if (Song6.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq6.Value
                };
                if (form6.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form6.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form6.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form6.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util6.Timer(sine20Seconds, Volume6, RateOfChange6, RangeLow6, RangeHigh6, freq6, Balance6);
            }
            else
            {
                util6.StopTimer();
            }
        }
        private void Song7_Checked(object sender, RoutedEventArgs e)
        {
            if (Song7.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq7.Value
                };
                if (form7.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form7.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form7.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form7.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util7.Timer(sine20Seconds, Volume7, RateOfChange7, RangeLow7, RangeHigh7, freq7, Balance7);
            }
            else
            {
                util7.StopTimer();
            }
        }
        private void Song9_Checked(object sender, RoutedEventArgs e)
        {
            if (Song9.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq8.Value
                };
                if (form9.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form9.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form9.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form9.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util9.Timer(sine20Seconds, Volume9, RateOfChange9, RangeLow9, RangeHigh9, freq9, Balance9);
            }
            else
            {
                util9.StopTimer();
            }
        }
        private void Song10_Checked(object sender, RoutedEventArgs e)
        {
            if (Song10.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq9.Value
                };
                if (form10.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form10.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form10.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form10.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util10.Timer(sine20Seconds, Volume10, RateOfChange10, RangeLow10, RangeHigh10, freq10, Balance10);
            }
            else
            {
                util10.StopTimer();
            }
        }
        private void Song11_Checked(object sender, RoutedEventArgs e)
        {
            if (Song11.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq10.Value
                };
                if (form11.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form11.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form11.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form11.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util11.Timer(sine20Seconds, Volume11, RateOfChange11, RangeLow11, RangeHigh11, freq11, Balance11);
            }
            else
            {
                util11.StopTimer();
            }
        }
        private void Song12_Checked(object sender, RoutedEventArgs e)
        {
            if (Song12.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq12.Value
                };
                if (form12.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form12.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form12.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form12.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util12.Timer(sine20Seconds, Volume12, RateOfChange12, RangeLow12, RangeHigh12, freq12, Balance12);
            }
            else
            {
                util12.StopTimer();
            }
        }
        private void Song13_Checked(object sender, RoutedEventArgs e)
        {
            if (Song13.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq13.Value
                };
                if (form13.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form13.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form13.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form13.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util13.Timer(sine20Seconds, Volume13, RateOfChange13, RangeLow13, RangeHigh13, freq13, Balance13);
            }
            else
            {
                util13.StopTimer();
            }
        }
        private void Song14_Checked(object sender, RoutedEventArgs e)
        {
            if (Song14.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq14.Value
                };
                if (form14.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form14.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form14.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form14.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util14.Timer(sine20Seconds, Volume14, RateOfChange14, RangeLow14, RangeHigh14, freq14, Balance14);
            }
            else
            {
                util14.StopTimer();
            }
        }
        private void Song15_Checked(object sender, RoutedEventArgs e)
        {
            if (Song15.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq15.Value
                };
                if (form15.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form15.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form15.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form15.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util15.Timer(sine20Seconds, Volume15, RateOfChange15, RangeLow15, RangeHigh15, freq15, Balance15);
            }
            else
            {
                util15.StopTimer();
            }
        }
        private void Song16_Checked(object sender, RoutedEventArgs e)
        {
            if (Song16.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = freq16.Value
                };
                if (form16.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form16.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form16.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form16.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util16.Timer(sine20Seconds, Volume16, RateOfChange16, RangeLow16, RangeHigh16, freq16, Balance16);
            }
            else
            {
                util16.StopTimer();
            }
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }
        private void Song8_Checked(object sender, RoutedEventArgs e)
        {
            if (Song8.IsChecked == true)
            {
                var sine20Seconds = new SignalGenerator()
                {
                    Gain = 0.5,
                    Frequency = Convert.ToDouble(freqBox8)
                };
                if (form8.Text == "Sine")
                    sine20Seconds.Type = SignalGeneratorType.Sin;
                if (form8.Text == "Square")
                    sine20Seconds.Type = SignalGeneratorType.Square;
                if (form8.Text == "Triangle")
                    sine20Seconds.Type = SignalGeneratorType.Triangle;
                if (form8.Text == "Saw")
                    sine20Seconds.Type = SignalGeneratorType.SawTooth;
                util8.Timer(sine20Seconds, Volume8, RateOfChange8, RangeLow8, RangeHigh8, freq8, Balance8);
            }
            else
            {
                util8.StopTimer();
            }
        }

        /// <summary>
        /// Save all the settings in a file
        /// </summary>
        private void Save()
        {
            #region Volume
            XElement element = new XElement("settings",
            new XElement("vol1", Volume1.Value),
            new XElement("vol2", Volume2.Value),
            new XElement("vol3", Volume3.Value),
            new XElement("vol4", Volume4.Value),
            new XElement("vol5", Volume5.Value),
            new XElement("vol6", Volume6.Value),
            new XElement("vol7", Volume7.Value),
            new XElement("vol8", Volume8.Value),
            new XElement("vol9", Volume9.Value),
            new XElement("vol10", Volume10.Value),
            new XElement("vol11", Volume11.Value),
            new XElement("vol12", Volume12.Value),
            new XElement("vol13", Volume13.Value),
            new XElement("vol14", Volume14.Value),
            new XElement("vol15", Volume15.Value),
            new XElement("vol16", Volume16.Value),
            #endregion
            #region Higer Volume Range
            new XElement("rangehigh1", RangeHigh1.Value.GetValueOrDefault()),
            new XElement("rangehigh2", RangeHigh2.Value.GetValueOrDefault()),
            new XElement("rangehigh3", RangeHigh3.Value.GetValueOrDefault()),
            new XElement("rangehigh4", RangeHigh4.Value.GetValueOrDefault()),
            new XElement("rangehigh5", RangeHigh5.Value.GetValueOrDefault()),
            new XElement("rangehigh6", RangeHigh6.Value.GetValueOrDefault()),
            new XElement("rangehigh7", RangeHigh7.Value.GetValueOrDefault()),
            new XElement("rangehigh8", RangeHigh8.Value.GetValueOrDefault()),
            new XElement("rangehigh9", RangeHigh9.Value.GetValueOrDefault()),
            new XElement("rangehigh10", RangeHigh10.Value.GetValueOrDefault()),
            new XElement("rangehigh11", RangeHigh11.Value.GetValueOrDefault()),
            new XElement("rangehigh12", RangeHigh12.Value.GetValueOrDefault()),
            new XElement("rangehigh13", RangeHigh13.Value.GetValueOrDefault()),
            new XElement("rangehigh14", RangeHigh14.Value.GetValueOrDefault()),
            new XElement("rangehigh15", RangeHigh15.Value.GetValueOrDefault()),
            new XElement("rangehigh16", RangeHigh16.Value.GetValueOrDefault()),
            #endregion
            #region Lower Volume Range
            new XElement("rangelow1", RangeLow1.Value.GetValueOrDefault()),
            new XElement("rangelow2", RangeLow2.Value.GetValueOrDefault()),
            new XElement("rangelow3", RangeLow3.Value.GetValueOrDefault()),
            new XElement("rangelow4", RangeLow4.Value.GetValueOrDefault()),
            new XElement("rangelow5", RangeLow5.Value.GetValueOrDefault()),
            new XElement("rangelow6", RangeLow6.Value.GetValueOrDefault()),
            new XElement("rangelow7", RangeLow7.Value.GetValueOrDefault()),
            new XElement("rangelow8", RangeLow8.Value.GetValueOrDefault()),
            new XElement("rangelow9", RangeLow9.Value.GetValueOrDefault()),
            new XElement("rangelow10", RangeLow10.Value.GetValueOrDefault()),
            new XElement("rangelow11", RangeLow11.Value.GetValueOrDefault()),
            new XElement("rangelow12", RangeLow12.Value.GetValueOrDefault()),
            new XElement("rangelow13", RangeLow13.Value.GetValueOrDefault()),
            new XElement("rangelow14", RangeLow14.Value.GetValueOrDefault()),
            new XElement("rangelow15", RangeLow15.Value.GetValueOrDefault()),
            new XElement("rangelow16", RangeLow16.Value.GetValueOrDefault()),
            #endregion
            #region Rate
            new XElement("rate1", RateOfChange1.Value),
            new XElement("rate2", RateOfChange2.Value),
            new XElement("rate3", RateOfChange3.Value),
            new XElement("rate4", RateOfChange4.Value),
            new XElement("rate5", RateOfChange5.Value),
            new XElement("rate6", RateOfChange6.Value),
            new XElement("rate7", RateOfChange7.Value),
            new XElement("rate8", RateOfChange8.Value),
            new XElement("rate9", RateOfChange9.Value),
            new XElement("rate10", RateOfChange10.Value),
            new XElement("rate11", RateOfChange11.Value),
            new XElement("rate12", RateOfChange12.Value),
            new XElement("rate13", RateOfChange13.Value),
            new XElement("rate14", RateOfChange14.Value),
            new XElement("rate15", RateOfChange15.Value),
            new XElement("rate16", RateOfChange16.Value),
            #endregion
            #region Balance
            new XElement("balance1", Balance1.Value),
            new XElement("balance2", Balance2.Value),
            new XElement("balance3", Balance3.Value),
            new XElement("balance4", Balance4.Value),
            new XElement("balance5", Balance5.Value),
            new XElement("balance6", Balance6.Value),
            new XElement("balance7", Balance7.Value),
            new XElement("balance8", Balance8.Value),
            new XElement("balance9", Balance9.Value),
            new XElement("balance10", Balance10.Value),
            new XElement("balance11", Balance11.Value),
            new XElement("balance12", Balance12.Value),
            new XElement("balance13", Balance13.Value),
            new XElement("balance14", Balance14.Value),
            new XElement("balance15", Balance15.Value),
            new XElement("balance16", Balance16.Value),
            #endregion
            #region Frequency
            new XElement("freq1", freq1.Value),
            new XElement("freq2", freq2.Value),
            new XElement("freq3", freq3.Value),
            new XElement("freq4", freq4.Value),
            new XElement("freq5", freq5.Value),
            new XElement("freq6", freq6.Value),
            new XElement("freq7", freq7.Value),
            new XElement("freq8", freq8.Value),
            new XElement("freq9", freq9.Value),
            new XElement("freq10", freq10.Value),
            new XElement("freq11", freq11.Value),
            new XElement("freq12", freq12.Value),
            new XElement("freq13", freq13.Value),
            new XElement("freq14", freq14.Value),
            new XElement("freq15", freq15.Value),
            new XElement("freq16", freq16.Value),
            #endregion
            #region Type
            new XElement("type1", form1.Text),
            new XElement("type2", form2.Text),
            new XElement("type3", form3.Text),
            new XElement("type4", form4.Text),
            new XElement("type5", form5.Text),
            new XElement("type6", form6.Text),
            new XElement("type7", form7.Text),
            new XElement("type8", form8.Text),
            new XElement("type9", form9.Text),
            new XElement("type10", form10.Text),
            new XElement("type11", form11.Text),
            new XElement("type12", form12.Text),
            new XElement("type13", form13.Text),
            new XElement("type14", form14.Text),
            new XElement("type15", form15.Text),
            new XElement("type16", form16.Text)
            #endregion
            );

            // Opening the file dialogue
            SaveFileDialog result = new SaveFileDialog();
            result.Filter = "Richard File|*.ric";
            result.AddExtension = true;
            result.DefaultExt = "ric";
            if (result.ShowDialog() == true)
            {
                element.Save(result.FileName);
                System.Windows.MessageBox.Show("Settings Saved Successfully.", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Load the settings from saved files.
        /// </summary>
        private void Load()
        {
            // File browser to select the file.
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Richard File|*.ric";
            if (openFile.ShowDialog() == true)
            {
                XElement element = XElement.Load(openFile.FileName);

                #region Songs
                Song1.Content = element.Element("song1").Value;
                Song2.Content = element.Element("song2").Value;
                Song3.Content = element.Element("song3").Value;
                Song4.Content = element.Element("song4").Value;
                Song5.Content = element.Element("song5").Value;
                Song6.Content = element.Element("song6").Value;
                Song7.Content = element.Element("song7").Value;
                Song8.Content = element.Element("song8").Value;
                Song9.Content = element.Element("song9").Value;
                Song10.Content = element.Element("song10").Value;
                Song11.Content = element.Element("song11").Value;
                Song12.Content = element.Element("song12").Value;
                Song13.Content = element.Element("song13").Value;
                Song14.Content = element.Element("song14").Value;
                Song15.Content = element.Element("song15").Value;
                Song16.Content = element.Element("song16").Value;
                #endregion
                #region Volume
                Volume1.Value = Convert.ToDouble(element.Element("vol1").Value);
                Volume2.Value = Convert.ToDouble(element.Element("vol2").Value);
                Volume3.Value = Convert.ToDouble(element.Element("vol3").Value);
                Volume4.Value = Convert.ToDouble(element.Element("vol4").Value);
                Volume5.Value = Convert.ToDouble(element.Element("vol5").Value);
                Volume6.Value = Convert.ToDouble(element.Element("vol6").Value);
                Volume7.Value = Convert.ToDouble(element.Element("vol7").Value);
                Volume8.Value = Convert.ToDouble(element.Element("vol8").Value);
                Volume9.Value = Convert.ToDouble(element.Element("vol9").Value);
                Volume10.Value = Convert.ToDouble(element.Element("vol10").Value);
                Volume11.Value = Convert.ToDouble(element.Element("vol11").Value);
                Volume12.Value = Convert.ToDouble(element.Element("vol12").Value);
                Volume13.Value = Convert.ToDouble(element.Element("vol13").Value);
                Volume14.Value = Convert.ToDouble(element.Element("vol14").Value);
                Volume15.Value = Convert.ToDouble(element.Element("vol15").Value);
                Volume16.Value = Convert.ToDouble(element.Element("vol16").Value);
                #endregion
                #region Higer Volume Range
                RangeHigh1.Value = Convert.ToInt32(element.Element("rangehigh1").Value);
                RangeHigh2.Value = Convert.ToInt32(element.Element("rangehigh2").Value);
                RangeHigh3.Value = Convert.ToInt32(element.Element("rangehigh3").Value);
                RangeHigh4.Value = Convert.ToInt32(element.Element("rangehigh4").Value);
                RangeHigh5.Value = Convert.ToInt32(element.Element("rangehigh5").Value);
                RangeHigh6.Value = Convert.ToInt32(element.Element("rangehigh6").Value);
                RangeHigh7.Value = Convert.ToInt32(element.Element("rangehigh7").Value);
                RangeHigh8.Value = Convert.ToInt32(element.Element("rangehigh8").Value);
                RangeHigh9.Value = Convert.ToInt32(element.Element("rangehigh9").Value);
                RangeHigh10.Value = Convert.ToInt32(element.Element("rangehigh10").Value);
                RangeHigh11.Value = Convert.ToInt32(element.Element("rangehigh11").Value);
                RangeHigh12.Value = Convert.ToInt32(element.Element("rangehigh12").Value);
                RangeHigh13.Value = Convert.ToInt32(element.Element("rangehigh13").Value);
                RangeHigh14.Value = Convert.ToInt32(element.Element("rangehigh14").Value);
                RangeHigh15.Value = Convert.ToInt32(element.Element("rangehigh15").Value);
                RangeHigh16.Value = Convert.ToInt32(element.Element("rangehigh16").Value);
                #endregion
                #region Lower Volume Range
                RangeLow1.Value = Convert.ToInt32(element.Element("rangelow1").Value);
                RangeLow2.Value = Convert.ToInt32(element.Element("rangelow2").Value);
                RangeLow3.Value = Convert.ToInt32(element.Element("rangelow3").Value);
                RangeLow4.Value = Convert.ToInt32(element.Element("rangelow4").Value);
                RangeLow5.Value = Convert.ToInt32(element.Element("rangelow5").Value);
                RangeLow6.Value = Convert.ToInt32(element.Element("rangelow6").Value);
                RangeLow7.Value = Convert.ToInt32(element.Element("rangelow7").Value);
                RangeLow8.Value = Convert.ToInt32(element.Element("rangelow8").Value);
                RangeLow9.Value = Convert.ToInt32(element.Element("rangelow9").Value);
                RangeLow10.Value = Convert.ToInt32(element.Element("rangelow10").Value);
                RangeLow11.Value = Convert.ToInt32(element.Element("rangelow11").Value);
                RangeLow12.Value = Convert.ToInt32(element.Element("rangelow12").Value);
                RangeLow13.Value = Convert.ToInt32(element.Element("rangelow13").Value);
                RangeLow14.Value = Convert.ToInt32(element.Element("rangelow14").Value);
                RangeLow15.Value = Convert.ToInt32(element.Element("rangelow15").Value);
                RangeLow16.Value = Convert.ToInt32(element.Element("rangelow16").Value);
                #endregion
                #region Rate
                RateOfChange1.Value = Convert.ToDouble(element.Element("rate1").Value);
                RateOfChange2.Value = Convert.ToDouble(element.Element("rate2").Value);
                RateOfChange3.Value = Convert.ToDouble(element.Element("rate3").Value);
                RateOfChange4.Value = Convert.ToDouble(element.Element("rate4").Value);
                RateOfChange5.Value = Convert.ToDouble(element.Element("rate5").Value);
                RateOfChange6.Value = Convert.ToDouble(element.Element("rate6").Value);
                RateOfChange7.Value = Convert.ToDouble(element.Element("rate7").Value);
                RateOfChange8.Value = Convert.ToDouble(element.Element("rate8").Value);
                RateOfChange9.Value = Convert.ToDouble(element.Element("rate9").Value);
                RateOfChange10.Value = Convert.ToDouble(element.Element("rate10").Value);
                RateOfChange11.Value = Convert.ToDouble(element.Element("rate11").Value);
                RateOfChange12.Value = Convert.ToDouble(element.Element("rate12").Value);
                RateOfChange13.Value = Convert.ToDouble(element.Element("rate13").Value);
                RateOfChange14.Value = Convert.ToDouble(element.Element("rate14").Value);
                RateOfChange15.Value = Convert.ToDouble(element.Element("rate15").Value);
                RateOfChange16.Value = Convert.ToDouble(element.Element("rate16").Value);
                #endregion
                #region Balance
                Balance1.Value = Convert.ToDouble(element.Element("balance1").Value);
                Balance2.Value = Convert.ToDouble(element.Element("balance2").Value);
                Balance3.Value = Convert.ToDouble(element.Element("balance3").Value);
                Balance4.Value = Convert.ToDouble(element.Element("balance4").Value);
                Balance5.Value = Convert.ToDouble(element.Element("balance5").Value);
                Balance6.Value = Convert.ToDouble(element.Element("balance6").Value);
                Balance7.Value = Convert.ToDouble(element.Element("balance7").Value);
                Balance8.Value = Convert.ToDouble(element.Element("balance8").Value);
                Balance9.Value = Convert.ToDouble(element.Element("balance9").Value);
                Balance10.Value = Convert.ToDouble(element.Element("balance10").Value);
                Balance11.Value = Convert.ToDouble(element.Element("balance11").Value);
                Balance12.Value = Convert.ToDouble(element.Element("balance12").Value);
                Balance13.Value = Convert.ToDouble(element.Element("balance13").Value);
                Balance14.Value = Convert.ToDouble(element.Element("balance14").Value);
                Balance15.Value = Convert.ToDouble(element.Element("balance15").Value);
                Balance16.Value = Convert.ToDouble(element.Element("balance16").Value);
                #endregion

                #region Frequency
                freq1.Value = Convert.ToDouble(element.Element("freq1").Value);
                freq2.Value = Convert.ToDouble(element.Element("freq2").Value);
                freq3.Value = Convert.ToDouble(element.Element("freq3").Value);
                freq4.Value = Convert.ToDouble(element.Element("freq4").Value);
                freq5.Value = Convert.ToDouble(element.Element("freq5").Value);
                freq6.Value = Convert.ToDouble(element.Element("freq6").Value);
                freq7.Value = Convert.ToDouble(element.Element("freq7").Value);
                freq8.Value = Convert.ToDouble(element.Element("freq8").Value);
                freq9.Value = Convert.ToDouble(element.Element("freq9").Value);
                freq10.Value = Convert.ToDouble(element.Element("freq10").Value);
                freq11.Value = Convert.ToDouble(element.Element("freq11").Value);
                freq12.Value = Convert.ToDouble(element.Element("freq12").Value);
                freq13.Value = Convert.ToDouble(element.Element("freq13").Value);
                freq14.Value = Convert.ToDouble(element.Element("freq14").Value);
                freq15.Value = Convert.ToDouble(element.Element("freq15").Value);
                freq16.Value = Convert.ToDouble(element.Element("freq16").Value);
                #endregion
                #region
                form1.Text = element.Element("type1").Value;
                form2.Text = element.Element("type2").Value;
                form3.Text = element.Element("type3").Value;
                form4.Text = element.Element("type4").Value;
                form5.Text = element.Element("type5").Value;
                form6.Text = element.Element("type6").Value;
                form7.Text = element.Element("type7").Value;
                form8.Text = element.Element("type8").Value;
                form9.Text = element.Element("type9").Value;
                form10.Text = element.Element("type10").Value;
                form11.Text = element.Element("type11").Value;
                form12.Text = element.Element("type12").Value;
                form13.Text = element.Element("type13").Value;
                form14.Text = element.Element("type14").Value;
                form15.Text = element.Element("type15").Value;
                form16.Text = element.Element("type16").Value;
                #endregion
                System.Windows.MessageBox.Show("Settings Loaded Successfully.", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


        /// <summary>
        /// Initialising the Players and its utilities.
        /// </summary>
        //public MediaPlayer mediaPlayer1 = new MediaPlayer();
        //public MediaPlayer mediaPlayer2 = new MediaPlayer();
        //public MediaPlayer mediaPlayer3 = new MediaPlayer();
        //public MediaPlayer mediaPlayer4 = new MediaPlayer();
        //public MediaPlayer mediaPlayer5 = new MediaPlayer();
        //public MediaPlayer mediaPlayer6 = new MediaPlayer();
        //public MediaPlayer mediaPlayer7 = new MediaPlayer();
        //public MediaPlayer mediaPlayer8 = new MediaPlayer();
        //public MediaPlayer mediaPlayer9 = new MediaPlayer();
        //public MediaPlayer mediaPlayer10 = new MediaPlayer();
        //public MediaPlayer mediaPlayer11 = new MediaPlayer();
        //public MediaPlayer mediaPlayer12 = new MediaPlayer();
        //public MediaPlayer mediaPlayer13 = new MediaPlayer();
        //public MediaPlayer mediaPlayer14 = new MediaPlayer();
        //public MediaPlayer mediaPlayer15 = new MediaPlayer();
        //public MediaPlayer mediaPlayer16 = new MediaPlayer();
        private Util util1 = new Util();
        private Util util2 = new Util();
        private Util util3 = new Util();
        private Util util4 = new Util();
        private Util util5 = new Util();
        private Util util6 = new Util();
        private Util util7 = new Util();
        private Util util8 = new Util();
        private Util util9 = new Util();
        private Util util10 = new Util();
        private Util util11 = new Util();
        private Util util12 = new Util();
        private Util util13 = new Util();
        private Util util14 = new Util();
        private Util util15 = new Util();
        private Util util16 = new Util();
        private SignalGenerator sine20Seconds;
        private DispatcherTimer timer;

        private void freq1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable1.Content = Math.Round(freq1.Value, 4).ToString() + " Hz";
        }

        private void freq2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable2.Content = Math.Round(freq2.Value, 4).ToString() + "Hz";
        }

        private void freq3_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable3.Content = Math.Round(freq3.Value, 2).ToString() + "Hz";
        }

        private void freq4_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable4.Content = Math.Round(freq4.Value, 2).ToString() + "Hz";
        }

        private void freq5_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable5.Content = Math.Round(freq5.Value, 2).ToString() + "Hz";
        }

        private void freq6_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable6.Content = Math.Round(freq6.Value, 2).ToString() + "Hz";
        }

        private void freq7_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable7.Content = Math.Round(freq7.Value, 2).ToString() + "Hz";
        }

        private void freq8_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable8.Content = Math.Round(freq8.Value, 2).ToString() + "Hz";
        }

        private void freq9_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable9.Content = Math.Round(freq9.Value, 2).ToString() + "Hz";
        }

        private void freq10_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable10.Content = Math.Round(freq10.Value, 2).ToString() + "Hz";
        }

        private void freq11_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable11.Content = Math.Round(freq11.Value, 2).ToString() + "Hz";
        }

        private void freq12_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable12.Content = Math.Round(freq12.Value, 2).ToString() + "Hz";
        }

        private void freq13_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable13.Content = Math.Round(freq13.Value, 2).ToString() + "Hz";
        }

        private void freq14_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable14.Content = Math.Round(freq14.Value, 2).ToString() + "Hz";
        }

        private void freq15_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable15.Content = Math.Round(freq15.Value, 2).ToString() + "Hz";
        }

        private void freq16_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //freqLable16.Content = Math.Round(freq16.Value, 2).ToString() + "Hz";
        }

        private void freqBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq1 != null && freqBox1.Text != "0" && freqBox1.Text != "")
            {
                if (Convert.ToDouble(freqBox1.Text) >= 500 && Convert.ToDouble(freqBox1.Text) <= 19500)
                {
                    freq1.Minimum = Convert.ToDouble(freqBox1.Text) - 500;
                    freq1.Maximum = Convert.ToDouble(freqBox1.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox1.Text) <= 500)
                {
                    freq1.Minimum = 0;
                    freq1.Maximum = Convert.ToDouble(freqBox1.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox1.Text) >= 19500)
                {
                    freq1.Minimum = Convert.ToDouble(freqBox1.Text) - 500;
                    freq1.Maximum = 20000;
                }
                freq1.Value = Convert.ToDouble(freqBox1.Text);
            }
        }
        private void freqBox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq2 != null && freqBox2.Text != "0" && freqBox2.Text != "")
            {
                if (Convert.ToDouble(freqBox2.Text) >= 500 && Convert.ToDouble(freqBox2.Text) <= 19500)
                {
                    freq2.Minimum = Convert.ToDouble(freqBox2.Text) - 500;
                    freq2.Maximum = Convert.ToDouble(freqBox2.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox2.Text) <= 500)
                {
                    freq2.Minimum = 0;
                    freq2.Maximum = Convert.ToDouble(freqBox2.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox2.Text) >= 19500)
                {
                    freq2.Minimum = Convert.ToDouble(freqBox2.Text) - 500;
                    freq2.Maximum = 20000;
                }
                freq2.Value = Convert.ToDouble(freqBox2.Text);
            }
        }
        private void freqBox3_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq3 != null && freqBox3.Text != "0" && freqBox3.Text != "")
            {
                if (Convert.ToDouble(freqBox3.Text) >= 500 && Convert.ToDouble(freqBox3.Text) <= 19500)
                {
                    freq3.Minimum = Convert.ToDouble(freqBox3.Text) - 500;
                    freq3.Maximum = Convert.ToDouble(freqBox3.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox3.Text) <= 500)
                {
                    freq3.Minimum = 0;
                    freq3.Maximum = Convert.ToDouble(freqBox3.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox3.Text) >= 19500)
                {
                    freq3.Minimum = Convert.ToDouble(freqBox3.Text) - 500;
                    freq3.Maximum = 20000;
                }
                freq3.Value = Convert.ToDouble(freqBox3.Text);
            }
        }
        private void freqBox4_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq4 != null && freqBox4.Text != "0" && freqBox4.Text != "")
            {
                if (Convert.ToDouble(freqBox4.Text) >= 500 && Convert.ToDouble(freqBox4.Text) <= 19500)
                {
                    freq4.Minimum = Convert.ToDouble(freqBox4.Text) - 500;
                    freq4.Maximum = Convert.ToDouble(freqBox4.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox4.Text) <= 500)
                {
                    freq4.Minimum = 0;
                    freq4.Maximum = Convert.ToDouble(freqBox4.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox4.Text) >= 19500)
                {
                    freq4.Minimum = Convert.ToDouble(freqBox4.Text) - 500;
                    freq4.Maximum = 20000;
                }
                freq4.Value = Convert.ToDouble(freqBox4.Text);
            }
        }
        private void freqBox5_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq5 != null && freqBox5.Text != "0" && freqBox5.Text != "")
            {
                if (Convert.ToDouble(freqBox5.Text) >= 500 && Convert.ToDouble(freqBox5.Text) <= 19500)
                {
                    freq5.Minimum = Convert.ToDouble(freqBox5.Text) - 500;
                    freq5.Maximum = Convert.ToDouble(freqBox5.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox5.Text) <= 500)
                {
                    freq5.Minimum = 0;
                    freq5.Maximum = Convert.ToDouble(freqBox5.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox5.Text) >= 19500)
                {
                    freq5.Minimum = Convert.ToDouble(freqBox5.Text) - 500;
                    freq5.Maximum = 20000;
                }
                freq5.Value = Convert.ToDouble(freqBox5.Text);
            }
        }
        private void freqBox6_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq6 != null && freqBox6.Text != "0" && freqBox6.Text != "")
            {
                if (Convert.ToDouble(freqBox6.Text) >= 500 && Convert.ToDouble(freqBox6.Text) <= 19500)
                {
                    freq6.Minimum = Convert.ToDouble(freqBox6.Text) - 500;
                    freq6.Maximum = Convert.ToDouble(freqBox6.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox6.Text) <= 500)
                {
                    freq6.Minimum = 0;
                    freq6.Maximum = Convert.ToDouble(freqBox6.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox6.Text) >= 19500)
                {
                    freq6.Minimum = Convert.ToDouble(freqBox6.Text) - 500;
                    freq6.Maximum = 20000;
                }
                freq6.Value = Convert.ToDouble(freqBox6.Text);
            }
        }
        private void freqBox7_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq7 != null && freqBox7.Text != "0" && freqBox7.Text != "")
            {
                if (Convert.ToDouble(freqBox7.Text) >= 500 && Convert.ToDouble(freqBox7.Text) <= 19500)
                {
                    freq7.Minimum = Convert.ToDouble(freqBox7.Text) - 500;
                    freq7.Maximum = Convert.ToDouble(freqBox7.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox7.Text) <= 500)
                {
                    freq7.Minimum = 0;
                    freq7.Maximum = Convert.ToDouble(freqBox7.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox7.Text) >= 19500)
                {
                    freq7.Minimum = Convert.ToDouble(freqBox7.Text) - 500;
                    freq7.Maximum = 20000;
                }
                freq7.Value = Convert.ToDouble(freqBox7.Text);
            }
        }
        private void freqBox8_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq8 != null && freqBox8.Text != "0" && freqBox8.Text != "")
            {
                if (Convert.ToDouble(freqBox8.Text) >= 500 && Convert.ToDouble(freqBox8.Text) <= 19500)
                {
                    freq8.Minimum = Convert.ToDouble(freqBox8.Text) - 500;
                    freq8.Maximum = Convert.ToDouble(freqBox8.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox8.Text) <= 500)
                {
                    freq8.Minimum = 0;
                    freq8.Maximum = Convert.ToDouble(freqBox8.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox8.Text) >= 19500)
                {
                    freq8.Minimum = Convert.ToDouble(freqBox8.Text) - 500;
                    freq8.Maximum = 20000;
                }
                freq8.Value = Convert.ToDouble(freqBox8.Text);
            }
        }
        private void freqBox9_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq9 != null && freqBox9.Text != "0" && freqBox9.Text != "")
            {
                if (Convert.ToDouble(freqBox9.Text) >= 500 && Convert.ToDouble(freqBox9.Text) <= 19500)
                {
                    freq9.Minimum = Convert.ToDouble(freqBox9.Text) - 500;
                    freq9.Maximum = Convert.ToDouble(freqBox9.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox9.Text) <= 500)
                {
                    freq9.Minimum = 0;
                    freq9.Maximum = Convert.ToDouble(freqBox9.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox9.Text) >= 19500)
                {
                    freq9.Minimum = Convert.ToDouble(freqBox9.Text) - 500;
                    freq9.Maximum = 20000;
                }
                freq9.Value = Convert.ToDouble(freqBox9.Text);
            }
        }
        private void freqBox10_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq10 != null && freqBox10.Text != "0" && freqBox10.Text != "")
            {
                if (Convert.ToDouble(freqBox10.Text) >= 500 && Convert.ToDouble(freqBox10.Text) <= 19500)
                {
                    freq10.Minimum = Convert.ToDouble(freqBox10.Text) - 500;
                    freq10.Maximum = Convert.ToDouble(freqBox10.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox10.Text) <= 500)
                {
                    freq10.Minimum = 0;
                    freq10.Maximum = Convert.ToDouble(freqBox10.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox10.Text) >= 19500)
                {
                    freq10.Minimum = Convert.ToDouble(freqBox10.Text) - 500;
                    freq10.Maximum = 20000;
                }
                freq10.Value = Convert.ToDouble(freqBox10.Text);
            }
        }
        private void freqBox11_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq11 != null && freqBox11.Text != "0" && freqBox11.Text != "")
            {
                if (Convert.ToDouble(freqBox11.Text) >= 500 && Convert.ToDouble(freqBox11.Text) <= 19500)
                {
                    freq11.Minimum = Convert.ToDouble(freqBox11.Text) - 500;
                    freq11.Maximum = Convert.ToDouble(freqBox11.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox11.Text) <= 500)
                {
                    freq11.Minimum = 0;
                    freq11.Maximum = Convert.ToDouble(freqBox11.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox11.Text) >= 19500)
                {
                    freq11.Minimum = Convert.ToDouble(freqBox11.Text) - 500;
                    freq11.Maximum = 20000;
                }
                freq11.Value = Convert.ToDouble(freqBox11.Text);
            }
        }
        private void freqBox12_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq12 != null && freqBox12.Text != "0" && freqBox12.Text != "")
            {
                if (Convert.ToDouble(freqBox12.Text) >= 500 && Convert.ToDouble(freqBox12.Text) <= 19500)
                {
                    freq12.Minimum = Convert.ToDouble(freqBox12.Text) - 500;
                    freq12.Maximum = Convert.ToDouble(freqBox12.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox12.Text) <= 500)
                {
                    freq12.Minimum = 0;
                    freq12.Maximum = Convert.ToDouble(freqBox12.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox12.Text) >= 19500)
                {
                    freq12.Minimum = Convert.ToDouble(freqBox12.Text) - 500;
                    freq12.Maximum = 20000;
                }
                freq12.Value = Convert.ToDouble(freqBox12.Text);
            }
        }
        private void freqBox13_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq13 != null && freqBox13.Text != "0" && freqBox13.Text != "")
            {
                if (Convert.ToDouble(freqBox13.Text) >= 500 && Convert.ToDouble(freqBox13.Text) <= 19500)
                {
                    freq13.Minimum = Convert.ToDouble(freqBox13.Text) - 500;
                    freq13.Maximum = Convert.ToDouble(freqBox13.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox13.Text) <= 500)
                {
                    freq13.Minimum = 0;
                    freq13.Maximum = Convert.ToDouble(freqBox13.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox13.Text) >= 19500)
                {
                    freq13.Minimum = Convert.ToDouble(freqBox13.Text) - 500;
                    freq13.Maximum = 20000;
                }
                freq13.Value = Convert.ToDouble(freqBox13.Text);
            }
        }
        private void freqBox14_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq14 != null && freqBox14.Text != "0" && freqBox14.Text != "")
            {
                if (Convert.ToDouble(freqBox14.Text) >= 500 && Convert.ToDouble(freqBox14.Text) <= 19500)
                {
                    freq14.Minimum = Convert.ToDouble(freqBox14.Text) - 500;
                    freq14.Maximum = Convert.ToDouble(freqBox14.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox14.Text) <= 500)
                {
                    freq14.Minimum = 0;
                    freq14.Maximum = Convert.ToDouble(freqBox14.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox14.Text) >= 19500)
                {
                    freq14.Minimum = Convert.ToDouble(freqBox14.Text) - 500;
                    freq14.Maximum = 20000;
                }
                freq14.Value = Convert.ToDouble(freqBox14.Text);
            }
        }
        private void freqBox15_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq15 != null && freqBox15.Text != "0" && freqBox15.Text != "")
            {
                if (Convert.ToDouble(freqBox15.Text) >= 500 && Convert.ToDouble(freqBox15.Text) <= 19500)
                {
                    freq15.Minimum = Convert.ToDouble(freqBox15.Text) - 500;
                    freq15.Maximum = Convert.ToDouble(freqBox15.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox15.Text) <= 500)
                {
                    freq15.Minimum = 0;
                    freq15.Maximum = Convert.ToDouble(freqBox15.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox15.Text) >= 19500)
                {
                    freq15.Minimum = Convert.ToDouble(freqBox15.Text) - 500;
                    freq15.Maximum = 20000;
                }
                freq15.Value = Convert.ToDouble(freqBox15.Text);
            }
        }
        private void freqBox16_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (freq16 != null && freqBox16.Text != "0" && freqBox16.Text != "")
            {
                if (Convert.ToDouble(freqBox16.Text) >= 500 && Convert.ToDouble(freqBox16.Text) <= 19500)
                {
                    freq16.Minimum = Convert.ToDouble(freqBox16.Text) - 500;
                    freq16.Maximum = Convert.ToDouble(freqBox16.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox16.Text) <= 500)
                {
                    freq16.Minimum = 0;
                    freq16.Maximum = Convert.ToDouble(freqBox16.Text) + 500;
                }
                else if (Convert.ToDouble(freqBox16.Text) >= 19500)
                {
                    freq16.Minimum = Convert.ToDouble(freqBox16.Text) - 500;
                    freq16.Maximum = 20000;
                }
                freq16.Value = Convert.ToDouble(freqBox16.Text);
            }
        }

        private void CheckUIChange()
        {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Tick += (a, b) => //Starts the timer
            {
                freqMeter1.Text = Math.Round(freq1.Value, 2).ToString() + "Hz";
                freqMeter2.Text = Math.Round(freq2.Value, 2).ToString() + "Hz";
                freqMeter3.Text = Math.Round(freq3.Value, 2).ToString() + "Hz";
                freqMeter4.Text = Math.Round(freq4.Value, 2).ToString() + "Hz";
                freqMeter5.Text = Math.Round(freq5.Value, 2).ToString() + "Hz";
                freqMeter6.Text = Math.Round(freq6.Value, 2).ToString() + "Hz";
                freqMeter7.Text = Math.Round(freq7.Value, 2).ToString() + "Hz";
                freqMeter8.Text = Math.Round(freq8.Value, 2).ToString() + "Hz";
                freqMeter9.Text = Math.Round(freq9.Value, 2).ToString() + "Hz";
                freqMeter10.Text = Math.Round(freq10.Value, 2).ToString() + "Hz";
                freqMeter11.Text = Math.Round(freq11.Value, 2).ToString() + "Hz";
                freqMeter12.Text = Math.Round(freq12.Value, 2).ToString() + "Hz";
                freqMeter13.Text = Math.Round(freq13.Value, 2).ToString() + "Hz";
                freqMeter14.Text = Math.Round(freq14.Value, 2).ToString() + "Hz";
                freqMeter15.Text = Math.Round(freq15.Value, 2).ToString() + "Hz";
                freqMeter16.Text = Math.Round(freq16.Value, 2).ToString() + "Hz";
            };
            timer.Start();
        }
    }
}
